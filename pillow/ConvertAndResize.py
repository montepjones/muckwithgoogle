from pathlib import Path
from PIL import Image

def rotate_and_resize(file_name):
    im = Image.open(file_name)
    # converting to jpg 
    rgb_im = im.convert("RGB") 
    out_file = Path(file_name)
    rgb_im.rotate(-90).resize((128,128)).save(f"/opt/icons/{out_file.name}")

def main():
    path = Path.home()
    images = path / "images"
    [rotate_and_resize(e) for e in images.rglob("ic*")]

if __name__ == "__main__":
    main()