
def solution(x, y):
    y_number = 1
    for number in range(1, y):
        y_number = y_number + number
    out_number = y_number
    for number in range(y + 1, x + y):
        out_number = out_number + number
    return str(out_number)

print(solution(5, 10))
print(solution(3, 2))

