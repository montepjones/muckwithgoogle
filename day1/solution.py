import bisect

def solution(area):
    squared_numbers = [number ** 2 for number in range(1, 1001)]
    area_list = []

    while area > 0:
        index = bisect.bisect(squared_numbers, area) 
        area_add_to_list = squared_numbers[index-1]
        area_list.append(area_add_to_list)
        area = area - area_add_to_list
    return ','.join(map(str, area_list))



