import json
# 
with open('car_sales.json', 'r') as file:
  data = json.load(file)
  file.close()
# # test ={x["car"]["car_year"]: x["total_sales"] for x in data if x["total_sales"]} 
# car_year_sales_list = [{x["car"]["car_year"]: x["total_sales"]} for x in data] 
# print(type(car_year_sales_list))
# test1 = {k: sum(d[k] for d in car_year_sales_list) for k in car_year_sales_list[0]}           
# print(test1)
# sum(x['amt'] for x in g)} 
from collections import defaultdict
c = defaultdict(int)
for d in data:
    c[d['car']["car_year"]] += d['total_sales']
print(c)
year, sales = max(c.items(), key=lambda a: a[1])
print(year, sales)
# dict1 = [{'a':2, 'b':3},{'a':3, 'b':4}]
# print(type(dict1))
# test1 = {k: sum(d[k] for d in dict1) for k in dict1[0]}           
# print(test1)