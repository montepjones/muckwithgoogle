import requests
from pathlib import Path
import os
def list_files(new_file):
    lines = open(new_file).read().splitlines()
    keys = ['title', 'name', 'date', 'feedback']
    post_dictionary = dict(zip(keys, lines))
    return post_dictionary

def main():
    """
    docstring
    """
    # path = Path.home()
    # file_list = path / "python-tuts/pillow/"
    testit = [list_files(e) for e in os.listdir("/home/mjones/python-tuts/pillow/testing")]
    print(os.listdir("/home/mjones/python-tuts/pillow/testing"))
    print(testit)

    # r = requests.get('http://localhost:3000/posts')
    # print(r.text)
    # r = requests.post('http://localhost:3000/posts', json=testit)
    # print(r.status_code)

if __name__ == "__main__":
    main()

